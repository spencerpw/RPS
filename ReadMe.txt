========================================================================
    RPS AI Battle Arena
========================================================================

Create your own RPS AI Player and test it against others.

This was written in around an hour when I should have been paying attention in class. 
It is meant for students at TriOS College in the Video Game Design & Development program.
As the TA I figured I'd make a fun exercise for the students, based on what my CS teacher did in Highscool while learning Java.
The source is very bad, and this whole thing should probably be written differently, but whatever it works.

Edit the YourNameRPS.h and YourNameRPS.cpp template files, edit the top of Main to add your player to the list.
Start() is called at the beginning of each match and is given the number of rounds that will be played. Use it to reset any tracking your AI is doing.
Win() Lose() and Tie() are called after the result of each round, use these to track moves and detect the opposing algorithm.
GetMove() is called at the start of each round and gives the arena your next play.

There are 4 sample AIs:
Random - plays a random move
Rock Always Wins - only ever plays rock
I'll get you next time - plays what would have beaten your last move
Never the same - randomly plays a move that is NOT the same as its last

The code is not error free in anyway, but it shouldn't break if you use it like a normal person.

E-mail: spencer@poonwoo.com
Twitter: @spensaurpw

