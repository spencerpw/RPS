#include "YourNameRPS.h"

/***
 * Default constructor
 ***/
YourNameRPS::YourNameRPS()
{
	m_sName = "Your Name";
}

/***
 * Called by arena to get your next move
 ***/
Player::Move YourNameRPS::GetMove(){
	return ROCK;
}

/***
 * Runs at the start of each match
 ***/
void YourNameRPS::Start(){

}

/***
 * Called upon winning
 ***/
void YourNameRPS::Win(){

}

/***
 * Called upon losing
 ***/
void YourNameRPS::Lose(){

}

/***
 * Called upon tie
 ***/
void YourNameRPS::Tie(){

}