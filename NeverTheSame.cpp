#include "StdAfx.h"
#include "NeverTheSame.h"

NeverTheSame::NeverTheSame()
{
	m_sName = "Never The Same";
	m_lastMove = ROCK;
}


NeverTheSame::~NeverTheSame(void)
{
}

Player::Move NeverTheSame::GetMove(){
	Move move;
	move = static_cast<Move>(rand()%3);
	while(move == m_lastMove){
		move = static_cast<Move>(rand()%3);
	}
	m_lastMove = move;
	return move;
	
}
