#pragma once

#include <iostream>
#include <string>

#include "Utilities.h"

using namespace std;

class Player
{
public:
	Player(string sName);
	Player();
	~Player();

	enum Move{
		ROCK,
		PAPER,
		SCISSORS
	};

	string sGetName();
	virtual Move GetMove();
	virtual void Start(int);
	virtual void Win();
	virtual void Lose();
	virtual void Tie();

protected:
	string m_sName;
};

