#include "BeatYourLast.h"

/*
Plays whatever would have beat the previous move of the opponent
*/
BeatYourLast::BeatYourLast(void)
{
	m_sName = "Get you next time";
	m_opponentPrev = SCISSORS;
}


BeatYourLast::~BeatYourLast(void)
{
}

Player::Move BeatYourLast::GetMove(){
	Move move;
	if(m_opponentPrev == ROCK){
		move = PAPER;
	}
	else if(m_opponentPrev == PAPER){
		move = SCISSORS;
	}
	else{
		move = ROCK;
	}
	m_myPrev = move;
	return move;
}

void BeatYourLast::Win(){
	if(m_myPrev == ROCK){
		m_opponentPrev = SCISSORS;
	}
	else if(m_myPrev == SCISSORS){
		m_opponentPrev = PAPER;
	}
	else if(m_myPrev == PAPER){
		m_opponentPrev = ROCK;
	}
}

void BeatYourLast::Lose(){
	if(m_myPrev == ROCK){
		m_opponentPrev = PAPER;
	}
	else if(m_myPrev == SCISSORS){
		m_opponentPrev =ROCK;
	}
	else if(m_myPrev == PAPER){
		m_opponentPrev = SCISSORS;
	}
}

void BeatYourLast::Tie(){
	m_opponentPrev = m_myPrev;
}

