#pragma once
#include "player.h"
/***
 *Template Player Subclass
 ***/
class Cyclops :public Player
{
public:
	Cyclops();
	Move GetMove();
	void Start(int);
	void Win();
	void Lose();
	void Tie();

private:
	int m_iMove;
};

