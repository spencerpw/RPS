#pragma once
#include "player.h"
/***
 *Template Player Subclass
 ***/
class YourNameRPS :public Player
{
public:
	YourNameRPS();
	Move GetMove();
	void Start();
	void Win();
	void Lose();
	void Tie();
};

