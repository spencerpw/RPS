#include "Cyclops.h"

/***
 * Default constructor
 ***/
Cyclops::Cyclops()
{
	m_sName = "Cyclops";
}

/***
 * Called by arena to get your next move
 ***/
Player::Move Cyclops::GetMove(){
	m_iMove++;
	if(m_iMove > 2){
		m_iMove = 0;
	}

	return static_cast<Move>(m_iMove);
}

/***
 * Runs at the start of each match
 ***/
void Cyclops::Start(int iRounds){
	m_iMove = 0;
}

/***
 * Called upon winning
 ***/
void Cyclops::Win(){

}

/***
 * Called upon losing
 ***/
void Cyclops::Lose(){

}

/***
 * Called upon tie
 ***/
void Cyclops::Tie(){

}