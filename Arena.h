#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "Player.h"
#include "Utilities.h"

using namespace std;

class Arena
{
public:
	Arena();
	~Arena();
	void vStart();
	void vAddPlayer(Player*);
	void vPlay(int);



private:
	Player* m_poFirstPlayer;
	Player* m_poSecondPlayer;

	vector<Player*> m_aPlayers;
	bool bSelectPlayers();
	void vPrintPlayers();
};

